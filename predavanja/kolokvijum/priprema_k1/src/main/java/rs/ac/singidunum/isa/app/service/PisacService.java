package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Pisac;
import rs.ac.singidunum.isa.app.repository.PisacRepository;

@Service
public class PisacService {
	@Autowired
	private PisacRepository pisacRepository;
	
	public Iterable<Pisac> findAll() {
		return pisacRepository.findAll();
	}
	
	public Optional<Pisac> findOne(Long id) {
		return pisacRepository.findById(id);
	}
	
	public Pisac save(Pisac korisnik) {
		return pisacRepository.save(korisnik);
	}
	
	public void delete(Long id) {
		pisacRepository.deleteById(id);
	}
	
	public void delete(Pisac korisnik) {
		pisacRepository.delete(korisnik);
	}
}
