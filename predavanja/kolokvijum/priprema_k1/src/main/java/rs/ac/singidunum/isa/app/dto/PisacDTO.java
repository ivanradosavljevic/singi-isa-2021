package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class PisacDTO {
	private Long id;
	private String ime;
	private String prezime;
	private ArrayList<KnjigaDTO> knjige = new ArrayList<KnjigaDTO>();

	public PisacDTO() {
		super();
	}

	public PisacDTO(Long id, String ime, String prezime, ArrayList<KnjigaDTO> knjige) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.knjige = knjige;
	}
	
	public PisacDTO(Long id, String ime, String prezime) {
		this(id, ime, prezime, new ArrayList<KnjigeDTO>());
	}

}
