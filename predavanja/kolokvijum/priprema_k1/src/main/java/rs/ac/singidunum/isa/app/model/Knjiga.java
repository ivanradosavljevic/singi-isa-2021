package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

@Entity
public class Knjiga {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naslov;
	private double cena;
	@Lob
	private String opis;
	
	@ManyToMany(mappedBy = "knjige")        // -------- PROVERITI !!!!!!!!!!!!!!!
	private Set<Pisac> autori = new HashSet<Pisac>();

	public Knjiga() {
		super();
	}

	public Knjiga(Long id, String naslov, double cena, String opis) {
		super();
		this.id = id;
		this.naslov = naslov;
		this.cena = cena;
		this.opis = opis;
	}

}
