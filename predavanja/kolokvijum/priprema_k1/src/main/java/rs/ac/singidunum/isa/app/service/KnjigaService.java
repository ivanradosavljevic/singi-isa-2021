package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Knjiga;
import rs.ac.singidunum.isa.app.repository.KnjigaRepository;

@Service
public class ArtikalService {
	@Autowired
	private KnjigaRepository knjigaRepository;

	public ArtikalService() {
		super();
	}

	public Iterable<Knjiga> findAll() {
		return knjigaRepository.findAll();
	}
	
	public Optional<Knjiga> findOne(Long id) {
		return knjigaRepository.findById(id);
	}
	
	// public List<Knjiga> findByPriceBetween(double min, double max) {
	// 	return knjigaRepository.pronadjiPoCeni(min, max);
	// }
	
	public Knjiga save(Knjiga artikal) {
		return knjigaRepository.save(artikal);
	}
	
	public void delete(Long id) {
		knjigaRepository.deleteById(id);
	}
	
	public void delete(Knjiga artikal) {
		knjigaRepository.delete(artikal);
	}
	
	// public boolean postaviPopust(Long id, double popust) {
	// 	Optional<Knjiga> artikal = knjigaRepository.findById(id);
	// 	if(artikal.isPresent()) {
	// 		artikal.get().setCena(artikal.get().getCena() - artikal.get().getCena()*popust);
	// 		knjigaRepository.save(artikal.get());
	// 		return true;
	// 	}
		
	// 	return false;
	// }
}
