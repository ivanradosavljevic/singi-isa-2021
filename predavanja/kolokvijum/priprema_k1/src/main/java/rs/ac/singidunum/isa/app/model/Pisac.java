package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Pisac {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String ime;
	@Column(nullable = false)
	private String prezime;
	
	@ManyToMany(mappedBy = "autori")     // ---              PAZLJIVO OPREZ !!!!!!
	private Set<Knjiga> knjige = new HashSet<Knjiga>();

	public Pisac() {
		super();
	}

	public Pisac(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

}
