package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class KnjigaDTO {
	private Long id;
	private String naslov;
	private String opis;
	private double cena;
	private ArrayList<PisacDTO> autori = new ArrayList<PisacDTO>();

	public KnjigaDTO() {
		super();
	}

	public KnjigaDTO(Long id, String naslov, String opis, double cena, ArrayList<PisacDTO> autori) {
		super();
		this.id = id;
		this.naslov = naslov;
		this.opis = opis;
		this.cena = cena;
		this.autori = autori;
	}
	
	public KnjigaDTO(Long id, String naslov, String opis, double cena) {
		this(id, naslov, opis, cena, new ArrayList<PisacDTO>());
	}


}
