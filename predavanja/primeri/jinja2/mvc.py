from jinja2 import Template

obj = {
    'name': 'user',
    'fields':[
        {'name': 'username', 'type': 'string'},
        {'name': 'firstname', 'type': 'string'}
    ]
}



template = """

public class {{obj.name.capitalize()}} {

    {% for el in obj.fields -%}
        {{el.type}} {{el.name}};
    {% endfor %}
}
"""

tm = Template(template)
msg = tm.render(obj=obj)

print(msg)