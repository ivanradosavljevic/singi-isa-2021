#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import url_for
from flask import Response
from flask import Flask, abort, send_file
from functools import wraps
import json
import jwt

app = Flask(__name__, static_folder='www')
APP_KEY = '1233432234.lozinkwwA'


@app.before_request
def before_request():
    request.__setattr__('user', None)
    token = request.headers.get('auth-token')
    try:
        obj = jwt.decode(token, APP_KEY, algorithms=['HS256'])
        request.__setattr__('user', obj)
    except:
        pass

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,auth-token')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response

def secured(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.user is None:
            abort(404)
            return 404
        return f(*args, **kwargs)
    return decorated_function


users = [
    {'username':'djordje', 'password': '123', 'admin':False},
    {'username':'admin', 'password': '1234', 'admin':True},
]


@app.route('/api/login/', methods=['PUT'])
def login():
    obj = request.json
    username = obj['username']
    password = obj['password']
    user = None
    for el in users:
        if el['username'] == username and el['password'] == password:
            user = el
            break
    if user is not None:
        token = jwt.encode({
            'user': el['username'],
            'isadmin': el['admin']
        }, APP_KEY, algorithm='HS256')
        return Response(json.dumps({'token': token.decode("utf-8") }))
    else:
        abort(404)
        return 404

@app.route('/api/students/', methods=["GET"])
@secured
def students():
    print('tu sam')
    return Response(json.dumps({'lista': 'abc'}), mimetype='application/json')


@app.route('/<path:path>')
def static_file(path):
    try:
        return app.send_static_file(path)
    except:
        return "Error"


@app.route('/')
def root():
    return app.send_static_file('index.html')




if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081, debug=True, use_reloader=False)

