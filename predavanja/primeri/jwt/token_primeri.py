
import base64

import numpy as np


encoded = base64.b64encode(b'data to be encoded')
print(encoded)
#'ZGF0YSB0byBiZSBlbmNvZGVk'
data = base64.b64decode(encoded)
print(data)
#'data to be encoded'


a = np.array([1, 2, 3, 4])
encoded = base64.b64encode(a)

data = base64.b64decode(encoded)
a = np.frombuffer(data, dtype=np.int64)
print(a)






import jwt

encoded = jwt.encode({'user': 'djobradovic', 'admin':True}, 'secret1234', algorithm='HS256')

decode = jwt.decode(encoded, 'secret1234', algorithms=['HS256'])

print(encoded)

print(decode)