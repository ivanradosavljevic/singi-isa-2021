package rs.ac.singidunum.isa.app.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.model.Prenos;
import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.PrenosRepository;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

@Service
public class PrenosService {
	@Autowired
	private PrenosRepository prenosRepository;
	
	@Autowired
	private RacunRepository racunRepository;

	public PrenosService() {
		super();
	}
	
	@Logged
	public void prenos(String brojRacunaUplatioca, String brojRacunaPrimaoca, Double iznos) {
		Optional<Racun> uplatilac = racunRepository.findById(brojRacunaUplatioca);
		Optional<Racun> primalac = racunRepository.findById(brojRacunaPrimaoca);
		
		if(uplatilac.isPresent() && primalac.isPresent()) {
			uplatilac.get().setStanje(uplatilac.get().getStanje()-iznos);
			primalac.get().setStanje(primalac.get().getStanje()+iznos);
			racunRepository.save(uplatilac.get());
			racunRepository.save(primalac.get());
			prenosRepository.save(new Prenos(null, uplatilac.get(), primalac.get(), LocalDateTime.now(), iznos));
		} else {
			
		}
	}
}
