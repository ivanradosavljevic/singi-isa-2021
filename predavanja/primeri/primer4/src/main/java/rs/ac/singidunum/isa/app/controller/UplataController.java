package rs.ac.singidunum.isa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.dto.PrenosDTO;
import rs.ac.singidunum.isa.app.service.PrenosService;
import rs.ac.singidunum.isa.app.service.RacunService;

@Controller
@RequestMapping(path = "/api/uplate")
public class UplataController {
	@Autowired
	RacunService racunService;
	@Autowired
	PrenosService prenosService;

	@Logged
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<PrenosDTO> createUplata(@RequestBody PrenosDTO prenos) {
		if (racunService.findOne(prenos.getUplatilac()) == null || racunService.findOne(prenos.getPrimalac()) == null) {
			return new ResponseEntity<PrenosDTO>(HttpStatus.NOT_FOUND);
		}
		prenosService.prenos(prenos.getUplatilac(), prenos.getPrimalac(), prenos.getIznos());
		return new ResponseEntity<PrenosDTO>(prenos, HttpStatus.OK);
	}
}
