package rs.ac.singidunum.isa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.dto.PrenosDTO;
import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.service.KorisnikService;

@RestController
@RequestMapping(path = "/api/korisnici")
@CrossOrigin
public class KorisnikController {
	@Autowired
	private KorisnikService korisnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<KorisnikDTO>> getAllKorisnici(Pageable pageable) {
		return new ResponseEntity<Page<KorisnikDTO>>(korisnikService.findAll(pageable).map(k->new KorisnikDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<KorisnikDTO> createKorisnik(@RequestBody Korisnik korisnik) {
		if (korisnikService.findOne(korisnik.getKorisnickoIme()) != null) {
			return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
		}
		Korisnik noviKorisnik = korisnikService.save(korisnik);
		return new ResponseEntity<KorisnikDTO>(new KorisnikDTO(noviKorisnik), HttpStatus.OK);
	}
	
	
}
