package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.RacunDTO;
import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.service.RacunService;

@Controller
@RequestMapping(path = "/api/racuni")
@CrossOrigin
public class RacunController {
	@Autowired
	private RacunService racunService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<RacunDTO>> getAllRacuni(@RequestParam(name = "minStanje", required = false) Double min,
			@RequestParam(name="maxStanje", required = false) Double max) {
		List<RacunDTO> racuni = new ArrayList<RacunDTO>();
		if(min == null) {
			min = -Double.MAX_VALUE;
		}
		if(max == null) {
			max = Double.MAX_VALUE;
		}
		for(Racun r : racunService.findAllByStanjeBetween(min, max)) {
			racuni.add(new RacunDTO(r));
		}
		return new ResponseEntity<List<RacunDTO>>(racuni, HttpStatus.OK);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.GET)
	public ResponseEntity<RacunDTO> getRacun(@PathVariable("brojRacuna") String brojRacuna) {
		Racun racun = racunService.findOne(brojRacuna);
		if (racun != null) {
			return new ResponseEntity<RacunDTO>(new RacunDTO(racun), HttpStatus.OK);
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Racun> createRacun(@RequestBody Racun noviRacun) {
		if (racunService.findOne(noviRacun.getBrojRacuna()) != null) {
			return new ResponseEntity<Racun>(HttpStatus.CONFLICT);
		}
		racunService.save(noviRacun);
		return new ResponseEntity<Racun>(noviRacun, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.PUT)
	public ResponseEntity<Racun> updateRacun(@PathVariable("brojRacuna") String brojRacuna,
			@RequestBody Racun podaciRacuna) {
		Racun racun = racunService.findOne(brojRacuna);
		if (racun == null) {
			return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
		}
		racun.setStanje(podaciRacuna.getStanje());
		racunService.save(racun);
		return new ResponseEntity<Racun>(racun, HttpStatus.OK);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRacun(@PathVariable("brojRacuna") String brojRacuna) {
		if (racunService.findOne(brojRacuna) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		racunService.delete(brojRacuna);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
