package rs.ac.singidunum.isa.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Racun;

@Repository
public interface RacunRepository extends CrudRepository<Racun, String> {
	public Iterable<Racun> findAllByStanjeBetween(Double min, Double max);
	
//	@Query("SELECT r FROM Racun r WHERE r.stanje BETWEEN ?1 and ?2")
//	public Iterable<Racun> findAllWhereStanjeBetween(Double min, Double max);
	
	@Query("SELECT r FROM Racun r WHERE r.stanje BETWEEN :min and :max")
	public Iterable<Racun> findAllWhereStanjeBetween(@Param("min") Double min, @Param("max") Double max);
}
