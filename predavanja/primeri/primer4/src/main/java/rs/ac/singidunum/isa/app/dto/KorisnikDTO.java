package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.model.PravoPristupa;
import rs.ac.singidunum.isa.app.model.Racun;

public class KorisnikDTO {
	private Long id;
	private String korisnickoIme;
	private String ime;
	private String prezime;

	private Set<RacunDTO> racuni = new HashSet<RacunDTO>();
	private List<PravoPristupaDTO> prava = new ArrayList<PravoPristupaDTO>();

	public KorisnikDTO() {
		super();
	}

	public KorisnikDTO(Korisnik korisnik) {
		super();
		this.id = korisnik.getId();
		this.korisnickoIme = korisnik.getKorisnickoIme();
		this.ime = korisnik.getIme();
		this.prezime = korisnik.getPrezime();
		for (Racun r : korisnik.getRacuni()) {
			racuni.add(new RacunDTO(r.getBrojRacuna(), r.getStanje(), null));
		}

		for (PravoPristupa p : korisnik.getPrava()) {
			prava.add(new PravoPristupaDTO(p.getId(), p.getNaziv()));
		}
	}

	public KorisnikDTO(Long id, String korisnickoIme, String ime, String prezime) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Set<RacunDTO> getRacuni() {
		return racuni;
	}

	public void setRacuni(Set<RacunDTO> racuni) {
		this.racuni = racuni;
	}

	public List<PravoPristupaDTO> getPrava() {
		return prava;
	}

	public void setPrava(List<PravoPristupaDTO> prava) {
		this.prava = prava;
	}
}
