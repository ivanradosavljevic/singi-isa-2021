package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	KorisnikRepository korisnikRepository;

	public KorisnikService() {
		super();
	}
	
	public Page<Korisnik> findAll(Pageable pageable) {
		return korisnikRepository.findAll(pageable);
	}
	
	public Korisnik findOne(String korisnickoIme) {
		Iterable<Korisnik> korisnici = korisnikRepository.findAllByKorisnickoIme(korisnickoIme);
		if(korisnici.iterator()!=null) {
			try {
				Korisnik ret = korisnici.iterator().next();
				return ret;				
			} catch (Exception e) {
				return null;
			}

		}else {
			return null;
		}
	}
	
	
	public Korisnik save(Korisnik korisnik) {
		return korisnikRepository.save(korisnik);
	}
	
}
