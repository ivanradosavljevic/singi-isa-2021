package rs.ac.singidunum.isa.app.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.PrenosDTO;

@Aspect
@Component
public class PrenosLogger {
	@Before("@annotation(Logged)")
	public void logPocetak(JoinPoint jp) {
		System.out.println("Zapoceto izvrsavanje metode: ");
		System.out.println(jp.getSignature());
		System.out.println("Argumenti metode: ");
		for(Object o : jp.getArgs()) {
			System.out.println(o);
		}
		System.out.println("---------------");
	}
	
	@Around("execution(* *.createUplata(..))")
	public void logOkoPrenosa(ProceedingJoinPoint jp) {
		System.out.println("Pocetak izvrsavanja: ");
		System.out.println(jp.getSignature());
		try {
			Object result = jp.proceed();
			System.out.println("Rezultat izvrsavanja: ");
			System.out.println(result);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("Kraj izvrsavanja: ");
		System.out.println(jp.getSignature());
		System.out.println("------------------");
	}
	
	@After("execution(* *.createUplata(rs.ac.singidunum.isa.app.dto.PrenosDTO)) && args(prenos,..)")
	public void logKrajPrenosa(PrenosDTO prenos) {
		System.out.println("Zavrsen prenos.");
		System.out.println(prenos);
		System.out.println("---------------");
	}
}
