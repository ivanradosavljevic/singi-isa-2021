-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: banka
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES ('admin@banka_primer.primer',1);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'Korisnik 1','korisnik1','korisnik1','Prezime korisnika 1'),(2,'Korisnik 2','korisnik2','korisnik2','Prezime korisnika 2'),(3,'Korisnik 3','korisnik3','korisnik3','Prezime korisnika 3'),(4,'Korisnik 4','korisnik4','korisnik4','Prezime korisnika 4');
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pravo_pristupa`
--

LOCK TABLES `pravo_pristupa` WRITE;
/*!40000 ALTER TABLE `pravo_pristupa` DISABLE KEYS */;
INSERT INTO `pravo_pristupa` VALUES (1,'Klijent'),(2,'Radnik'),(3,'Administrator');
/*!40000 ALTER TABLE `pravo_pristupa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pravo_pristupa_korisnici`
--

LOCK TABLES `pravo_pristupa_korisnici` WRITE;
/*!40000 ALTER TABLE `pravo_pristupa_korisnici` DISABLE KEYS */;
INSERT INTO `pravo_pristupa_korisnici` VALUES (3,1),(2,2),(1,3),(1,4);
/*!40000 ALTER TABLE `pravo_pristupa_korisnici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `prenos`
--

LOCK TABLES `prenos` WRITE;
/*!40000 ALTER TABLE `prenos` DISABLE KEYS */;
INSERT INTO `prenos` VALUES (1,'2020-01-15 17:45:21',170,'123948192475920332','231001230041230412');
/*!40000 ALTER TABLE `prenos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `racun`
--

LOCK TABLES `racun` WRITE;
/*!40000 ALTER TABLE `racun` DISABLE KEYS */;
INSERT INTO `racun` VALUES ('123948192475920332',3002,3),('231001230041230412',1030,3),('662301294958012039',10000,4);
/*!40000 ALTER TABLE `racun` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-19  6:25:10
