package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.dto.KupovinaDTO;
import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.service.KorisnikService;

@Controller
@RequestMapping(path = "/api/korisnici")
public class KorisnikController {
	@Autowired
	KorisnikService korisnikService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<KorisnikDTO>> getAllKorisnici(Pageable pageable) {
		Page<Korisnik> korisnici = korisnikService.findAll(pageable);
		
		return new ResponseEntity<Page<KorisnikDTO>>(
				korisnici.map(k -> new KorisnikDTO(k.getId(), k.getKorisnickoIme(), null,
						(ArrayList<KupovinaDTO>) k.getKupovine().stream()
								.map(kupovina -> new KupovinaDTO(kupovina.getId(), kupovina.getDatumKupovine()))
								.collect(Collectors.toList()))),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/{korisnikId}", method = RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getKorisnik(@PathVariable("korisnikId") Long korisnikId) {
		Optional<Korisnik> korisnik = korisnikService.findOne(korisnikId);

		KorisnikDTO korisnikDTO;

		if (korisnik.isPresent()) {

			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
			for (Kupovina kupovina : korisnik.get().getKupovine()) {
				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(),
						kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
			}

			korisnikDTO = new KorisnikDTO(korisnik.get().getId(), korisnik.get().getKorisnickoIme(),
					korisnik.get().getLozinka(), kupovine);

			return new ResponseEntity<KorisnikDTO>(korisnikDTO, HttpStatus.OK);
		}
		return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Korisnik> createKorisnik(@RequestBody Korisnik korisnik) {
		try {
			korisnikService.save(korisnik);
			return new ResponseEntity<Korisnik>(korisnik, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Korisnik>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(path = "/{korisnikId}", method = RequestMethod.PUT)
	public ResponseEntity<Korisnik> updateKorisnik(@PathVariable("korisnikId") Long korisnikId,
			@RequestBody Korisnik izmenjeniKorisnik) {
		Korisnik korisnik = korisnikService.findOne(korisnikId).orElse(null);
		if (korisnik != null) {
			izmenjeniKorisnik.setId(korisnikId);
			izmenjeniKorisnik = korisnikService.save(izmenjeniKorisnik);
			return new ResponseEntity<Korisnik>(izmenjeniKorisnik, HttpStatus.OK);
		}
		return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{korisnikId}", method = RequestMethod.DELETE)
	public ResponseEntity<Korisnik> deleteKorisnik(@PathVariable("korisnikId") Long korisnikId) {
		if (korisnikService.findOne(korisnikId).isPresent()) {
			korisnikService.delete(korisnikId);
			return new ResponseEntity<Korisnik>(HttpStatus.OK);
		}
		return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
	}
}
