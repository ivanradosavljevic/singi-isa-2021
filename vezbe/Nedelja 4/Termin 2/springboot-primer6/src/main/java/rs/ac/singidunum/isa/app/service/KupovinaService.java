package rs.ac.singidunum.isa.app.service;

import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.repository.KupovinaRepository;

@Service
public class KupovinaService {
	@Autowired
	private ArtikalService artikalService;
	
	@Autowired
	KupovinaRepository kupovinaRepository;
	
	public Iterable<Kupovina> findAll() {
		return kupovinaRepository.findAll();
	}
	
	public Optional<Kupovina> findOne(Long id) {
		return kupovinaRepository.findById(id);
	}
	
	public Kupovina save(Kupovina kupovina) {
		return kupovinaRepository.save(kupovina);
	}
	
	public void delete(Long id) {
		kupovinaRepository.deleteById(id);
	}
	
	public void delete(Kupovina kupovina) {
		kupovinaRepository.delete(kupovina);
	}
	
	@Transactional
	public void postaviPopust(Long id) {
		Optional<Kupovina> kupovina = findOne(id);
		artikalService.postaviPopust(kupovina.get().getArtikal().getId(), 0.5);
		if(true) {
			throw new RuntimeException("Greska u toku obrade transakcije");
		}
		kupovina.get().setDatumKupovine(new Date());
	}
}
