package rs.ac.singidunum.isa.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PrimerController {
	@RequestMapping(path = "/primer", method = RequestMethod.GET)
	public ResponseEntity<String> getPrimer() {
		return new ResponseEntity<String>("Instanca 2", HttpStatus.OK);
	}
}
