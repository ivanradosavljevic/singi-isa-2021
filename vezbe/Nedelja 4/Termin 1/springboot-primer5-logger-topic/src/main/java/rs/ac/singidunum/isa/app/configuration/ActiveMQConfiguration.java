package rs.ac.singidunum.isa.app.configuration;

import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActiveMQConfiguration {
	@Bean
	public Topic potvrdeTopic() {
		return new ActiveMQTopic("potvrde");
	}
}
