package rs.ac.singidunum.isa.app.aspect;

import java.time.LocalDateTime;

import javax.jms.Queue;
import javax.jms.Topic;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogDTO;

@Aspect
@Component
public class KupovinaLogger {
	@Autowired
	private Queue logQueue;
	
	@Autowired
	private Topic logTopic;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	/*
	 * Izvršavanje pre svake metode koja je anotirana anotacijom Logged.
	 */
	@Before("@annotation(Logged)")
	public void logPocetakIzvrsavanjaAnotacija(JoinPoint jp) {
		System.out.println("Pre izvrsavanja metode. [Logged]");
		System.out.println(jp.getSignature());
		for (Object o : jp.getArgs()) {
			System.out.println(o);
		}
		jmsTemplate.convertAndSend(logQueue, new LogDTO("info", jp.getSignature().toString(), LocalDateTime.now()));
		jmsTemplate.convertAndSend(logTopic, new LogDTO("info-topic", jp.getSignature().toString(), LocalDateTime.now()));
	}

	/*
	 * Izvršavanje pre svake metode koja je iz klase KupovinaController, čija povratna
	 * vrednost može biti bilo kog tipa i koja može primati proizvoljan broj argumenata
	 * proizvoljnog tipa.
	 */
	@Before("execution(* rs.ac.singidunum.isa.app.controller.KupovinaController.*(..))")
	public void logPocetakIzvrsavanja(JoinPoint jp) {
		System.out.println("Pre izvrsavanja metode.");
		System.out.println(jp.getSignature());
		for (Object o : jp.getArgs()) {
			System.out.println(o);
		}
	}

	/*
	 * Izvršavanje pre i nakon svake metode iz klase KupovinaController koja može imati
	 * proizvoljan tip povratne vrednosti i koja prima jedan argument tipa Long.
	 */
	@Around("execution(* rs.ac.singidunum.isa.app.controller.KupovinaController.*(Long))")
	public Object logOkoIzvrsavanja(ProceedingJoinPoint jp) {
		System.out.println("Pre izvrsavanja metode. [AROUND].");
		System.out.println(jp.getSignature());
		try {
			// Pribavljanje prosleđenih argumenata.
			Object[] args = jp.getArgs();
			
			/*
			 * Ukoliko je vrednost argumenta veća od 0 vraća
			 * se rezultat poziva metode.
			 */
			if ((Long) args[0] > 0) {
				// Prosleđuju se argumenti i nastavlja se izvršavanje presretnute metode.
				Object result = jp.proceed(args);
				return result;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("Nakon izvrsavanja metode. [AROUND].");
		/*
		 * U slučaju da prosleđena vrednost nije bila veća od 0 ili
		 * da je došlo do izuzetka u toku nastvljanja izvršavanja
		 * vraća se response entity sa bad request statusom.
		 */
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

	/*
	 * Izvršavanje nakon svake metode iz klase KupovinaController čiji naziv je proizvoljan
	 * i koja prima jedan argument tipa Long. Argument tipa long vezan je za ime id i može
	 * mu se pristupiti u metodi logKrajIzvrsavanja preko parametra tipa Long pod nazivom
	 * id.
	 */
	@After("execution(* rs.ac.singidunum.isa.app.controller.KupovinaController.*(Long)) && args(id,..)")
	public void logKrajIzvrsavanja(JoinPoint jp, Long id) {
		System.out.println(jp.getSignature());
		System.out.println("Nakon izvrsavanja metode.");
		System.out.println(id);
	}
}
