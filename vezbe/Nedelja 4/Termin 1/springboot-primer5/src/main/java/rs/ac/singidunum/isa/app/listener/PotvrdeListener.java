package rs.ac.singidunum.isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogDTO;

@Component
public class PotvrdeListener {
	@JmsListener(destination = "potvrde")
	public void onPotvrda(LogDTO log) {
		System.out.println(log);
	}
}
