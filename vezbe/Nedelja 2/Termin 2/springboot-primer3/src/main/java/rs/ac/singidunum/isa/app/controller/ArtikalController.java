package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.dto.ArtikalDTO;
import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.dto.KupovinaDTO;
import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.service.ArtikalService;

@Controller
@RequestMapping(path = "/api/artikli")
public class ArtikalController {
	@Autowired
	private ArtikalService artikalService;

	@Logged
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ArtikalDTO>> getAllArtikli(@RequestParam(name = "min", required = false) Double min,
			@RequestParam(name = "max", required = false) Double max) {
		if (min == null) {
			min = -Double.MAX_VALUE;
		}

		if (max == null) {
			max = Double.MAX_VALUE;
		}

		ArrayList<ArtikalDTO> artikli = new ArrayList<ArtikalDTO>();
		for (Artikal artikal : artikalService.findByPriceBetween(min, max)) {
			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
			for (Kupovina kupovina : artikal.getKupovine()) {
				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(),
						kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
			}
			artikli.add(new ArtikalDTO(artikal.getId(), artikal.getNaziv(), artikal.getOpis(), artikal.getCena(),
					kupovine));
		}

		return new ResponseEntity<Iterable<ArtikalDTO>>(artikli, HttpStatus.OK);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.GET)
	public ResponseEntity<ArtikalDTO> getArtikal(@PathVariable("artikalId") Long artikalId) {
		Optional<Artikal> artikal = artikalService.findOne(artikalId);
		ArtikalDTO artikalDTO;
		if (artikal.isPresent()) {
			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
			for (Kupovina kupovina : artikal.get().getKupovine()) {
				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(),
						kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
			}
			artikalDTO = new ArtikalDTO(artikal.get().getId(), artikal.get().getNaziv(), artikal.get().getOpis(),
					artikal.get().getCena(), kupovine);
			return new ResponseEntity<ArtikalDTO>(artikalDTO, HttpStatus.OK);
		}
		return new ResponseEntity<ArtikalDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Artikal> createArtikal(@RequestBody Artikal artikal) {
		try {
			artikalService.save(artikal);
			return new ResponseEntity<Artikal>(artikal, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Artikal>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> updateArtikal(@PathVariable("artikalId") Long artikalId,
			@RequestBody Artikal izmenjeniArtikal) {
		Artikal artikal = artikalService.findOne(artikalId).orElse(null);
		if (artikal != null) {
			izmenjeniArtikal.setId(artikalId);
			izmenjeniArtikal = artikalService.save(izmenjeniArtikal);
			return new ResponseEntity<Artikal>(izmenjeniArtikal, HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.DELETE)
	public ResponseEntity<Artikal> deleteArtikal(@PathVariable("artikalId") Long artikalId) {
		if (artikalService.findOne(artikalId).isPresent()) {
			artikalService.delete(artikalId);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}
}
