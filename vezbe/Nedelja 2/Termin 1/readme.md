# Vežbe 1

**Cilj vežbi:** Implementirati Spring Boot aplikaciju za prodaju proizvoda.

1. Napistati model za artikle, korisnike i kupljene proizvode.
2. Namapirati modele na tabele u relacionoj bazi podataka.
3. Napraviti repozitorijume za pristup podacima.
4. Napraviti kontrolere za sve entitete.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-data/jpa/docs/current/reference/html.
___