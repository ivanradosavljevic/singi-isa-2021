package rs.ac.singidunum.isa.app.dto;

import java.util.Date;

public class KupovinaDTO {
	private Long id;
	private ArtikalDTO artikal;
	private KorisnikDTO korisnik;
	private Date datumKupovine;

	public KupovinaDTO() {
		super();
	}

	public KupovinaDTO(Long id, ArtikalDTO artikal, KorisnikDTO korisnik, Date datumKupovine) {
		super();
		this.id = id;
		this.artikal = artikal;
		this.korisnik = korisnik;
		this.datumKupovine = datumKupovine;
	}

	public KupovinaDTO(Long id, Date datumKupovine) {
		this(id, null, null, datumKupovine);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumKupovine() {
		return datumKupovine;
	}

	public void setDatumKupovine(Date datumKupovine) {
		this.datumKupovine = datumKupovine;
	}

	public ArtikalDTO getArtikal() {
		return artikal;
	}

	public void setArtikal(ArtikalDTO artikal) {
		this.artikal = artikal;
	}

	public KorisnikDTO getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikDTO korisnik) {
		this.korisnik = korisnik;
	}
}
