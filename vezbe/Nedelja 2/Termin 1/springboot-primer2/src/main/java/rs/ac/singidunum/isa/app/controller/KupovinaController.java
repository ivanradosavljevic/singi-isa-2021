package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.ArtikalDTO;
import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.dto.KupovinaDTO;
import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.service.KupovinaService;

@Controller
@RequestMapping(path = "/api/kupovine")
public class KupovinaController {
	@Autowired
	KupovinaService kupovinaService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KupovinaDTO>> getAllKupovine() {
		ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();

		for (Kupovina kupovina : kupovinaService.findAll()) {
			kupovine.add(new KupovinaDTO(kupovina.getId(),
					new ArtikalDTO(kupovina.getArtikal().getId(), kupovina.getArtikal().getNaziv(),
							kupovina.getArtikal().getOpis(), kupovina.getArtikal().getCena()),
					new KorisnikDTO(kupovina.getKorisnik().getId(), kupovina.getKorisnik().getKorisnickoIme(), null),
					kupovina.getDatumKupovine()));
		}

		return new ResponseEntity<Iterable<KupovinaDTO>>(kupovine, HttpStatus.OK);
	}

	@RequestMapping(path = "/{kupovinaId}", method = RequestMethod.GET)
	public ResponseEntity<KupovinaDTO> getKupovina(@PathVariable("kupovinaId") Long kupovinaId) {
		Optional<Kupovina> kupovina = kupovinaService.findOne(kupovinaId);
		if (kupovina.isPresent()) {
			KupovinaDTO kupovinaDTO = new KupovinaDTO(kupovina.get().getId(),
					new ArtikalDTO(kupovina.get().getArtikal().getId(), kupovina.get().getArtikal().getNaziv(),
							kupovina.get().getArtikal().getOpis(), kupovina.get().getArtikal().getCena()),
					new KorisnikDTO(kupovina.get().getKorisnik().getId(),
							kupovina.get().getKorisnik().getKorisnickoIme(), null),
					kupovina.get().getDatumKupovine());
			return new ResponseEntity<KupovinaDTO>(kupovinaDTO, HttpStatus.OK);
		}
		return new ResponseEntity<KupovinaDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Kupovina> createKupovina(@RequestBody Kupovina kupovina) {
		try {
			kupovinaService.save(kupovina);
			return new ResponseEntity<Kupovina>(kupovina, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Kupovina>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(path = "/{kupovinaId}", method = RequestMethod.PUT)
	public ResponseEntity<Kupovina> updateKupovina(@PathVariable("kupovinaId") Long kupovinaId,
			@RequestBody Kupovina izmenjenaKupovina) {
		Kupovina kupovina = kupovinaService.findOne(kupovinaId).orElse(null);
		if (kupovina != null) {
			izmenjenaKupovina.setId(kupovinaId);
			izmenjenaKupovina = kupovinaService.save(izmenjenaKupovina);
			return new ResponseEntity<Kupovina>(izmenjenaKupovina, HttpStatus.OK);
		}
		return new ResponseEntity<Kupovina>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{kupovinaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Kupovina> deleteKupovina(@PathVariable("kupovinaId") Long kupovinaId) {
		if (kupovinaService.findOne(kupovinaId).isPresent()) {
			kupovinaService.delete(kupovinaId);
			return new ResponseEntity<Kupovina>(HttpStatus.OK);
		}
		return new ResponseEntity<Kupovina>(HttpStatus.NOT_FOUND);
	}
}
