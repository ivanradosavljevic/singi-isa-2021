package rs.ac.singidunum.isa.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import rs.ac.singidunum.isa.app.configuration.AppConfiguration;
import rs.ac.singidunum.isa.app.view.ArtikalView;

public class App {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class)) {
			ArtikalView artikalView = context.getBean(ArtikalView.class);
			artikalView.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
