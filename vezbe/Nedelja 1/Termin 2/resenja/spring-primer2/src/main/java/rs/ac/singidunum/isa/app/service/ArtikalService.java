package rs.ac.singidunum.isa.app.service;

import java.util.List;

import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.repository.ArtikalRepository;

public class ArtikalService {
	private ArtikalRepository artikalRepository;

	public ArtikalService() {
		super();
	}

	public ArtikalService(ArtikalRepository artikalRepository) {
		super();
		this.artikalRepository = artikalRepository;
	}

	public ArtikalRepository getArtikalRepository() {
		return artikalRepository;
	}

	public void setArtikalRepository(ArtikalRepository artikalRepository) {
		this.artikalRepository = artikalRepository;
	}
	
	public List<Artikal> findAll() {
		return artikalRepository.findAll();
	}
	
	public Artikal findOne(String id) {
		return artikalRepository.findOne(id);
	}
	
	public List<Artikal> findByPriceBetween(double min, double max) {
		return artikalRepository.findByPriceBetween(min, max);
	}
	
	public boolean save(Artikal artikal) {
		return artikalRepository.save(artikal);
	}
	
	public boolean update(Artikal artikal) {
		return artikalRepository.update(artikal);
	}
	
	public boolean delete(String id) {
		return artikalRepository.delete(id);
	}
	
	public boolean delete(Artikal artikal) {
		return artikalRepository.delete(artikal);
	}
	
	public boolean postaviPopust(String id, double popust) {
		Artikal artikal = artikalRepository.findOne(id);
		if(artikal != null) {
			artikal.setCena(artikal.getCena() - artikal.getCena()*popust);
			artikalRepository.update(artikal);
			return true;
		}
		
		return false;
	}
}
