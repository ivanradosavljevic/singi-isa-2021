package rs.ac.singidunum.isa.app.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rs.ac.singidunum.isa.app.repository.ArtikalRepository;
import rs.ac.singidunum.isa.app.service.ArtikalService;
import rs.ac.singidunum.isa.app.view.ArtikalView;

@Configuration
public class AppConfiguration {
	@Bean
	public ArtikalRepository artikalRepository() {
		return new ArtikalRepository();
	}
	
	@Bean
	public ArtikalService artikalService(ArtikalRepository artikalRepository) {
		return new ArtikalService(artikalRepository);
	}
	
	@Bean
	public ArtikalView artikalView(ArtikalService artikalService) {
		return new ArtikalView(artikalService);
	}
}
