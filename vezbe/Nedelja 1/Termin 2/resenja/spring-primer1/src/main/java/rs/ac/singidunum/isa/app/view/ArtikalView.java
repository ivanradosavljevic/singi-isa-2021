package rs.ac.singidunum.isa.app.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.service.ArtikalService;

public class ArtikalView {
	private ArtikalService artikalService;

	public ArtikalView() {
		super();
	}

	public ArtikalView(ArtikalService artikalService) {
		super();
		this.artikalService = artikalService;
	}

	public ArtikalService getArtikalService() {
		return artikalService;
	}

	public void setArtikalService(ArtikalService artikalService) {
		this.artikalService = artikalService;
	}
	
	protected void glavniMeni() {
		System.out.println("Izaberite opciju:");
		System.out.println("1. Prikaz svih artikala");
		System.out.println("2. Prikaz po id-ju");
		System.out.println("3. Dodavanje artikla");
		System.out.println("4. Izmena artikla");
		System.out.println("5. Uklanjanje artikla");
		System.out.println("6. Definisanje popusta");
		System.out.println("0. Izlaz");
	}
	
	protected String ocitajUlaz() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected void prikazSvih() {
		List<Artikal> artikli = artikalService.findAll();
		for(Artikal a : artikli) {
			System.out.println(a.getId() + "\t" + a.getNaziv() + "\t" + a.getCena());
		}
	}
	
	protected void prikazJednog() {
		System.out.println("Unesite id artikla: ");
		Artikal artikal = artikalService.findOne(ocitajUlaz());
		if(artikal != null) {
			System.out.println(artikal.getId() + "\t" + artikal.getNaziv() + "\t" + artikal.getCena());
		} else {
			System.out.println("Artikal nije pronadjen!");
		}
	}
	
	protected void dodajArtikal() {
		Artikal artikal = new Artikal();
		System.out.println("Unesite id artikla: ");
		artikal.setId(ocitajUlaz());
		System.out.println("Unesite naziv artikla: ");
		artikal.setNaziv(ocitajUlaz());
		System.out.println("Unesite cena artikla: ");
		artikal.setCena(Double.parseDouble(ocitajUlaz()));
		System.out.println("Unesite opis artikla: ");
		artikal.setOpis(ocitajUlaz());
		
		if(artikalService.save(artikal)) {
			System.out.println("Uspesno zapisan artikal!");
		} else {
			System.out.println("Artikal nije zapisan!");
		}
	}
	
	protected void izmeniArtikal() {
		Artikal artikal = new Artikal();
		System.out.println("Unesite id artikla: ");
		artikal.setId(ocitajUlaz());
		System.out.println("Unesite naziv artikla: ");
		artikal.setNaziv(ocitajUlaz());
		System.out.println("Unesite cena artikla: ");
		artikal.setCena(Double.parseDouble(ocitajUlaz()));
		System.out.println("Unesite opis artikla: ");
		artikal.setOpis(ocitajUlaz());
		
		if(artikalService.update(artikal)) {
			System.out.println("Uspesno zapisan artikal!");
		} else {
			System.out.println("Artikal nije zapisan!");
		}
	}
	
	protected void ukloniArtikal() {
		System.out.println("Unesite id artikla za brisanje: ");
		if(artikalService.delete(ocitajUlaz())) {
			System.out.println("Brisanje uspesno!");
		} else {
			System.out.println("Brisanje neuspesno!");
		}
	}
	
	protected void postaviPopust() {
		System.out.println("Unesite id artikla: ");
		String id = ocitajUlaz();
		System.out.println("Unesite visinu popusta: ");
		double popust = Double.parseDouble(ocitajUlaz());
		
		if(artikalService.postaviPopust(id, popust)) {
			System.out.println("Popust uspesno postavljen!");
		} else {
			System.out.println("Postavljanje popusta neuspesno!");
		}
	}
	
	public void show() {
		int izbor = 0;
		do {
			glavniMeni();
			
			try {
				izbor = Integer.parseInt(ocitajUlaz());
			} catch(Exception e) {
				izbor = -1;
			}
			
			switch (izbor) {
			case 1:
				prikazSvih();
				break;
			case 2:
				prikazJednog();
				break;
			case 3:
				dodajArtikal();
				break;
			case 4:
				izmeniArtikal();
				break;
			case 5:
				ukloniArtikal();
				break;
			case 6:
				postaviPopust();
				break;
			}
		} while(izbor != 0);
	}
}
