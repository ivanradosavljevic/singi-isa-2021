package rs.ac.singidunum.isa.app;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import rs.ac.singidunum.isa.app.view.ArtikalView;

public class App {

	public static void main(String[] args) {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")) {
			ArtikalView artikalView = context.getBean(ArtikalView.class);
			artikalView.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
