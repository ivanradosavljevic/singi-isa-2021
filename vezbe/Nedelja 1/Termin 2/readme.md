# Vežbe 2

**Cilj vežbi:** Implementacija poslovne aplikacije uz pomoć Spring radnog okvira.

1. Kreirati novi Maven projekat i uključiti zavisnosti za jezgro Spring radnog okvira, iskonfigurisati plugin za pakovanje kompajliranih klasa u jar datoteke.
2. Napisati klasu koja predstavlja artikal u radnji. Artikal je opisan nazivom, cenom, opisom i identifikatorom.
3. Napisati klasu koja predstavlja repozitorijum artikala, preko repozitorijuma je moguće dobaviti sve artikle, dobaviti jedan artikal po identifikatoru, dobaviti artikle po opsegu cene, uskladištiti nove artikle, izmeniti i obrisati postojeće artikle.
4. Napisati klasu u kojoj se implementira poslovna logika za artikle. Artikle je moguće, pregledati, dodavati, uklanjati, izmeniti i pretraživati po ceni. Pored ovih operacija artikle je moguće staviti na akciju, prilikom stavljanja na akciju artiklu se cena umanjuje za procenat koji korisnik definiše u trenutku stavljanja proizvoda na akciju.
5. Napisati klasu za prikaz artikala i pristup metodama poslovne logike kroz konzolni korisnički interfejs.
6. Napraviti Spring aplikaciju koja koristi prethodno navedene klase za realizaciju aplikacije za upravljanje artiklima.
___
### Dodatne napomene:
* Eclipse EE se može naći na adresi: https://www.eclipse.org/downloads/packages/.
* Dokumentacija za jezgro Spring radnog okvira: https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#spring-core.
___