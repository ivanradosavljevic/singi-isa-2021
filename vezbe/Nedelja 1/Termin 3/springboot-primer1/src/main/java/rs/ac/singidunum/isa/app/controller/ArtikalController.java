package rs.ac.singidunum.isa.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.service.ArtikalService;

@Controller
@RequestMapping(path = "/api/artikli")
public class ArtikalController {
	@Autowired
	private ArtikalService artikalService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<Artikal>> getAllArtikli(@RequestParam(name = "min", required = false) Double min,
			@RequestParam(name = "max", required = false) Double max) {
		if(min == null) {
			min = -Double.MAX_VALUE;
		}
		
		if(max == null) {
			max = Double.MAX_VALUE;
		}

		return new ResponseEntity<List<Artikal>>(artikalService.findByPriceBetween(min, max), HttpStatus.OK);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.GET)
	public ResponseEntity<Artikal> getArtikal(@PathVariable("artikalId") String artikalId) {
		Artikal artikal = artikalService.findOne(artikalId);
		if (artikal != null) {
			return new ResponseEntity<Artikal>(artikal, HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Artikal> createArtikal(@RequestBody Artikal artikal) {
		if (artikalService.save(artikal)) {
			return new ResponseEntity<Artikal>(artikal, HttpStatus.CREATED);
		}
		return new ResponseEntity<Artikal>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> updateArtikal(@PathVariable("artikalId") String artikalId,
			@RequestBody Artikal izmenjeniArtikal) {
		if (artikalService.findOne(artikalId) != null) {
			izmenjeniArtikal.setId(artikalId);
			artikalService.update(izmenjeniArtikal);
			return new ResponseEntity<Artikal>(izmenjeniArtikal, HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{artikalId}", method = RequestMethod.DELETE)
	public ResponseEntity<Artikal> deleteArtikal(@PathVariable("artikalId") String artikalId) {
		if (artikalService.findOne(artikalId) != null) {
			artikalService.delete(artikalId);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}
}
