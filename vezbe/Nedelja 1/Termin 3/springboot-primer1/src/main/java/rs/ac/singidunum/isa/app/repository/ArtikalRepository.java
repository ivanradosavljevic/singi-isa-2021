package rs.ac.singidunum.isa.app.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Artikal;

@Repository
public class ArtikalRepository {
	private HashMap<String, Artikal> artikli;

	public ArtikalRepository() {
		super();
		this.artikli = new HashMap<String, Artikal>();
		this.artikli.put("1", new Artikal("1", "1", -10, "1"));
		this.artikli.put("2", new Artikal("2", "2", 20, "2"));
		this.artikli.put("3", new Artikal("3", "3", 30, "3"));
	}
	
	public List<Artikal> findAll() {
		return new ArrayList<Artikal>(artikli.values());
	}
	
	public Artikal findOne(String id) {
		return artikli.get(id);
	}
	
	public boolean save(Artikal artikal) {
		if(artikli.containsKey(artikal.getId())) {
			return false;
		}
		
		artikli.put(artikal.getId(), artikal);
		return true;
	}
	
	public boolean delete(String id) {
		if(!artikli.containsKey(id)) {
			return false;
		}
		
		artikli.remove(id);
		
		return true;
	}
	
	public boolean delete(Artikal artikal) {
		return delete(artikal.getId());
	}
	
	public boolean update(Artikal artikal) {
		if(!artikli.containsKey(artikal.getId())) {
			return false;
		}
		
		artikli.replace(artikal.getId(), artikal);
		
		return true;
	}
	
	public List<Artikal> findByPriceBetween(double min, double max) {
		ArrayList<Artikal> filtrirani = new ArrayList<Artikal>();
		
		for(Artikal a : artikli.values()) {
			if(a.getCena() >= min && a.getCena() <= max) {
				filtrirani.add(a);
			}
		}
		
		return filtrirani;
	}
}
