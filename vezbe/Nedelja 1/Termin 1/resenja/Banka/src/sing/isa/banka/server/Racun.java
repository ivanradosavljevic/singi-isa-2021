package sing.isa.banka.server;

/*
 * Klasa koja opisuje jedan racun u banci.
 */
public class Racun {
	double raspolozivo;

	public Racun(double raspolozivo) {
		this.raspolozivo = raspolozivo;
	}

	public double getRaspolozivo() {
		return raspolozivo;
	}

	public void setRaspolozivo(double raspolozivo) {
		this.raspolozivo = raspolozivo;
	}
}
