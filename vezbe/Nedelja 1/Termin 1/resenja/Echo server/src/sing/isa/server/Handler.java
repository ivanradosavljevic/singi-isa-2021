package sing.isa.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/*
 * Klasa koja predstavlja obradjivac zahteva.
 * Mora implementirati interfejs Runnable kako
 * bi se objekti ove klase mogli proslediti
 * thread-u na izvrsavanje.
 */
public class Handler implements Runnable {
	// Cuva referencu na prosledjeni socket
	// radi dalje obrade.
	private Socket socket;

	// Konstruktor koji prima referencu na socket
	// klijenta.
	public Handler(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			// PrintWriter za zapisivanje odgovora.
			PrintWriter os = new PrintWriter(socket.getOutputStream());
			// BufferedReader za citanje sadrzaja primljene poruke.
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// Ispis adrese klijenta.
			System.out.println("Konekcija pristigla od: " + socket.getInetAddress().toString());
			// Ispis sadrzaja poruke.
			System.out.println("Sadrzaj: " + in.readLine());
			// Slanje odgovora klijentu.
			os.println("PONG");
			os.flush();
			// Zatvaranje tokova za zapisivanje i citanje.
			os.close();
			in.close();
			// Zatvaranje veze ka klijentu.
			socket.close();
			// Pauziranje izvrsavanja trenutnog thread-a radi simulacije duge obrade.
			Thread.sleep(10000);
		} catch (IOException | InterruptedException e) {
			// Ukoliko dodje do greske prilikom obrade zahteva
			// ispisuje se stack trace greske.
			e.printStackTrace();
		}
	}

}
