package sing.isa.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * Implementacija servera koji zahteve obradjuje
 * redom. Svaki zahtev blokira dalju obradu zahteva
 * sve do trenutka dok se obrada trenutnog zahteva
 * ne zavrsi.
 */
public class Server {
	public static void main(String[] args) throws InterruptedException {
		// Port na kojem server slusa.
		int port = 3000;
		
		// Otvaranje novog ServerSocket-a.
		try(ServerSocket ss = new ServerSocket(port)) {
			// Potvrda da je server uspesno pokrenut.
			System.out.println("Server sluša na port: " + port);

			// Beskonacna petlja za obradu zahteva.
			while (true) {
				// Server prima novi zahtev i prihvata ga na obradu.
				Socket socket = ss.accept();
				// PrintWriter za zapisivanje odgovora.
				PrintWriter os = new PrintWriter(socket.getOutputStream());
				// BufferedReader za citanje sadrzaja primljene poruke.
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				// Ispis adrese klijenta.
				System.out.println("Konekcija pristigla od: " + socket.getInetAddress().toString());
				// Ispis sadrzaja poruke.
				System.out.println("Sadrzaj poruke: " + in.readLine());
				// Slanje odgovora klijentu.
				os.println("PONG");
				os.flush();
				// Zatvaranje tokova za zapisivanje i citanje.
				os.close();
				in.close();
				// Zatvaranje veze ka klijentu.
				socket.close();
				// Pauziranje izvrsavanja trenutnog thread-a radi simulacije duge obrade.
				Thread.sleep(10000);
			}
		} catch (IOException e) {
			// U slucaju da dodje do neuspesnog instanciranja ServerSocket-a
			// ili do neuspesnog ispisa ili citanja podataka iz tokova
			// ispisuje se stack trace i poruka o gresci.
			e.printStackTrace();
			System.out.println("Neuspešno pokretanje servera.");
		}
	}
}
